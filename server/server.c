#include "myrpc.h"

int main(){

	SVCXPRT *transp;
	transp = svctcp_create(RPC_ANYSOCK, 100000, 100000);
	
	pmap_unset(PROG_PROCESS_REQ, VERS_PROCESS_REQ);
	pmap_unset(PROG_INFO, VERS_INFO);
	pmap_unset(PROG_KILL, VERS_KILL);
	pmap_unset(PROG_RUN, VERS_RUN);

	if(!svc_register(transp, PROG_PROCESS_REQ, VERS_PROCESS_REQ, sdf_all_process_request, IPPROTO_TCP)){
		fprintf(stderr, "can't register\n");
		exit(1);
	}

	if(!svc_register(transp, PROG_INFO, VERS_INFO, sdf_info_request, IPPROTO_TCP)){
		fprintf(stderr, "can't register\n");
		exit(1);
	}

	if(!svc_register(transp, PROG_KILL, VERS_KILL, sdf_kill_process, IPPROTO_TCP)){
		fprintf(stderr, "can't register\n");
		exit(1);
	}

	if(!svc_register(transp, PROG_RUN, VERS_RUN, sdf_run_process, IPPROTO_TCP)){
		fprintf(stderr, "can't register\n");
		exit(1);
	}

	printf("%s\n", "Сервер начал работать");
	svc_run();
	fprintf(stderr, "you don't have to reach this point");
	return 0;
}
