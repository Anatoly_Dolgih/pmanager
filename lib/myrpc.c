#include "myrpc.h"

/*********************************************************************************************************************/
/*                                        Клиентские функции                                                         */
/*********************************************************************************************************************/
/*
 *  Инициализация структуры timeval
 *  (для задания времени ожидания ответа)
 *  Инициализация клиентской структуры CLIENT
 *  (для получения данных о TCP соединении
 *  для получения PID, NAME и STATUS);
*/
bool client_init_connection(CLIENT** client, char* hostname, u_long prog_num,
                            u_long vers_num)
{
    struct hostent *hp;
    struct sockaddr_in server_addr;
    int sock = RPC_ANYSOCK;

    hp = gethostbyname(hostname);
    if(hp == NULL)
        return false;

    bcopy(hp->h_addr, &server_addr.sin_addr, hp->h_length);
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = 0;

    if((*client = clnttcp_create(&server_addr, prog_num, vers_num, &sock, BUF_SIZE, BUF_SIZE)) == NULL){
         clnt_pcreateerror("clnt_create");
         return false;
    }
    return true;
}

bool check_ping(char* hostname, char* wait_time, char* pack_number){
    char command[256] = "ping -c ";
    char show_code[15] = "; echo $?;";
    char answer[128];

    strcat(command, pack_number);
    strcat(command, " -w ");
    strcat(command, wait_time);
    strcat(command, " ");
    strcat(command, hostname);
    strcat(command, show_code);

    FILE* f = popen(command, "r");
    while(fgets(answer, sizeof(answer), f)){
        printf("%s\n", answer);
    }
    pclose(f);
    if(answer[0] == '0'){
        return true;
    }
    else{
        return false;
    }
}


/*  Получение данных обо всех процессах на сервере
 *  (PID, NAME, STATUS)
 *  успех = возвращаем строку с данными
 *  неудача = возвращаем nullptr  */
char* client_all_process_req(struct timeval* total_timeout, CLIENT** client){

    enum clnt_stat clnt_stat;
    static char r[BUF_SIZE];
    static char* pres = r;
    char *str = (char*)malloc(BUF_SIZE);
    memset(str, '0', 1);
    clnt_stat = clnt_call(*client, PROC_PROCESS_REQ, (xdrproc_t) xdr_void, 0, (xdrproc_t) xdr_wrapstring,
                          (char*) &str, *total_timeout);
    strcpy(r, str);
    if(clnt_stat != RPC_SUCCESS){
        clnt_perror(*client, "rpc");
        return NULL;
    }
    free(str);
    return pres;
}

/* Функция для получения информации о сервере
 * (данные о процессоре и памяти)
 * успех = возвращаем строку с данными
 * неудача = возвращаем nullptr  */
char* client_info_request(struct timeval* total_timeout, CLIENT** client){

    enum clnt_stat clnt_stat;

    static char r[BUF_SIZE];
    static char* pres = r;
    char *str = (char*)malloc(BUF_SIZE);
    memset(str, '0', 1);
    clnt_stat = clnt_call(*client, PROC_INFO, (xdrproc_t) xdr_void, 0, (xdrproc_t) xdr_wrapstring,
                          (char*) &str, *total_timeout);
    strcpy(r, str);
    if(clnt_stat != RPC_SUCCESS){
        clnt_perror(*client, "rpc");
        return NULL;
    }
    free(str);
    return pres;
}

/* Разрыв соединения */
void client_destroy_connection(CLIENT **client){
    if(*client != NULL)
        clnt_destroy(*client);
}

/*   Функция для уничтожения процесса на сервере   */
int client_kill(struct timeval* total_timeout, CLIENT** client, char* name){

    enum clnt_stat clnt_stat;
    static int res;
    char *str = (char*)malloc(BUF_SIZE);
    strcpy(str, name);
    //int *p_res = &res;
    memset((char*)&res, 0, sizeof(res));
    clnt_stat = clnt_call(*client, PROC_KILL, (xdrproc_t) xdr_wrapstring, (char*) &str, (xdrproc_t) xdr_int,
                          (caddr_t) &res, *total_timeout);
    if(clnt_stat != RPC_SUCCESS){
        clnt_perror(*client, "rpc");
        return ERROR_CLNT_CALL;
    }
    return res;
}


/*   Функция для запуска процесса на сервере   */
int client_run(struct timeval *total_timeout, CLIENT **client, char *name)
{
    enum clnt_stat clnt_stat;
    static int res;
    char *str = (char*)malloc(BUF_SIZE);
    strcpy(str, name);
    //int *p_res = &res;
    memset((char*)&res, 0, sizeof(res));
    clnt_stat = clnt_call(*client, PROC_RUN, (xdrproc_t) xdr_wrapstring, (char*) &str, (xdrproc_t) xdr_int,
                          (caddr_t) &res, *total_timeout);

    if(clnt_stat != RPC_SUCCESS){
        clnt_perror(*client, "rpc");
        return ERROR_CLNT_CALL;
    }
    return res;
}



/*********************************************************************************************************************/
/*                                             Серверные функции                                                     */
/*********************************************************************************************************************/
int IsNumeric(const char* CharacterList){
        for ( ; *CharacterList; CharacterList++)
        if (*CharacterList < '0' || *CharacterList > '9')
            return 0;
    return 1;
}

void sdf_all_process_request(struct svc_req *rqstp, SVCXPRT *transp)
{
    switch(rqstp->rq_proc){
        case NULLPROC:{
            if(!svc_sendreply(transp, (xdrproc_t) xdr_void, (caddr_t) 0)){
                printf("Ошибка отправки ответа (svc_sendreply)\n");
                return;
            }
        }
        case PROC_PROCESS_REQ:{
            struct dirent* procData = NULL;
            DIR* dir_proc = NULL;
            char* result = (char*)malloc(100000 * sizeof(char));
            memset(result, '\0', 1);

            dir_proc = opendir(PROC_DIRECTORY);
            if (dir_proc == NULL){
                perror("Couldn't open the " PROC_DIRECTORY " directory");
                return;
            }
            while ((procData = readdir(dir_proc)) != 0) {
                if (procData->d_type == DT_DIR) {
                    if (IsNumeric(procData->d_name)) {
                        char path[100] = PROC_DIRECTORY;
                        strcat(path, procData->d_name);
                        strcat(path, "/status");
                        FILE* statLineFile;
                        if((statLineFile = fopen(path, "r")) != NULL){
                            char name[50];
                            char status[30];
                            char r_uid[30];
                            char VmSize[100];
                            char VmRSS[100];

                            fscanf(statLineFile, "%s", name);
                            memset(name, '0', 50);
                            fscanf(statLineFile, "%s", name);
                            fgets(status, 30, statLineFile);

                            fgets(status, 30, statLineFile);
                            fscanf(statLineFile, "%s", status);
                            fgets(status, 30, statLineFile);

                            for(int i = 0; i < 6; i++){
                                fgets(r_uid, 30, statLineFile);
                            }
                            fscanf(statLineFile, "%s", r_uid);
                            memset(r_uid, '0', 30);
                            fscanf(statLineFile, "%s", r_uid);

                            for(int i = 0; i < 8; i++){
                                fgets(VmSize, 100, statLineFile);
                            }
                            fscanf(statLineFile, "%s", VmSize);
                            fgets(VmSize, 100, statLineFile);

                            for(int i = 0; i < 3; i++){
                                fgets(VmRSS, 100, statLineFile);
                            }
                            fscanf(statLineFile, "%s", VmRSS);
                            fgets(VmRSS, 100, statLineFile);

                            strcat(result, r_uid);
                            strcat(result, "\n\t");
                            strcat(result, procData->d_name);
                            strcat(result, "\n");
                            strcat(result, VmSize);
                            strcat(result, VmRSS);
                            strcat(result, status);
                            strcat(result, "\t");
                            strcat(result, name);
                            strcat(result, "\n\t");
                            fclose(statLineFile);


                        }
                    }
                }
            }
            closedir(dir_proc);
            if(!svc_sendreply(transp, (xdrproc_t) xdr_wrapstring, (char*) &result)){
                free(result);
                printf("Ошибка отправки ответа (svc_sendreply)\n");
                return;
            }
            printf("sdf_all_process_request() успешно выполнена\n");
            free(result);
            return;
        }
        default:{
            svcerr_noproc(transp);
            return;
        }
    }
}

void sdf_info_request(struct svc_req *rqstp, SVCXPRT *transp)
{
    switch(rqstp->rq_proc){
        case NULLPROC:{
            if(!svc_sendreply(transp, (xdrproc_t) xdr_void, (caddr_t) 0)){
                printf("Ошибка отправки ответа (svc_sendreply)\n");
                return;
            }
        }
        case PROC_INFO:{
            char* result = (char*)malloc(BUF_SIZE);
            char path[30] = PROC_DIRECTORY;
            strcat(path, "cpuinfo");
            char str[BUF_SIZE];
            FILE* cpu_info_file;

            if((cpu_info_file = fopen(path, "r")) != NULL){
                for(int i = 0; i < 4; i++){
                    memset(str, 0, BUF_SIZE);
                    fgets(str, BUF_SIZE, cpu_info_file);
                }
                memset(str, 0, BUF_SIZE);
                fgets(str, BUF_SIZE, cpu_info_file);
                fclose(cpu_info_file);
            }
            strcpy(result, str);
            FILE* mem_info_file;
            memset(path, 0, 30);
            strcpy(path, PROC_DIRECTORY);
            strcat(path, "meminfo");
            if((mem_info_file = fopen(path, "r")) != NULL){
                memset(str, 0, BUF_SIZE);
                fgets(str, BUF_SIZE, cpu_info_file);
                strcat(result, str);
                fclose(mem_info_file);
            }
            if(!svc_sendreply(transp, (xdrproc_t) xdr_wrapstring, (char*) &result)){
                free(result);
                printf("Ошибка отправки ответа (svc_sendreply)\n");
                return;
            }
            printf("svc_info request() успешно выполнена\n");
            free(result);
            return;
        }
        default:{
            svcerr_noproc(transp);
            return;
        }
    }
}

void sdf_kill_process(struct svc_req *rqstp, SVCXPRT *transp){
    switch(rqstp->rq_proc){
        case NULLPROC:{
            if(!svc_sendreply(transp, (xdrproc_t) xdr_void, (caddr_t) 0)){
                printf("Ошибка отправки ответа (svc_sendreply)\n");
                return;
            }
        }
        case PROC_KILL:{
            char* name = (char*)malloc(200);
            memset(name, '\0', 100);
            svc_getargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
            struct dirent* procData = NULL;
            DIR* dir_proc = NULL;
            static int res;
            memset((char*)&res, 0, sizeof(res));

            dir_proc = opendir(PROC_DIRECTORY);
            if (dir_proc == NULL){
                perror("Couldn't open the " PROC_DIRECTORY " directory");
                memset((char*)&res, WRONG_DIRECTORY, sizeof(res));
                svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
                free(name);
                if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &res)){
                    printf("Ошибка отправки ответа (svc_sendreply)\n");
                    return;
                }
                return;
            }

            while ((procData = readdir(dir_proc)) != 0) {
                if (procData->d_type == DT_DIR) {
                    if (IsNumeric(procData->d_name)) {

                        int check = strcmp(procData->d_name, name);

                        if(check == 0){
                            closedir(dir_proc);
                            int pid = atoi(procData->d_name);
                            //printf("%s%d\n", "Answer: ", pid);
                            if(kill(pid, SIGTERM) == -1){
                                res = ERROR_KILL;
                                svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
                                free(name);
                                if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &res)){
                                    printf("Ошибка отправки ответа (svc_sendreply)\n");
                                    return;
                                }
                                printf("Ошибка kill\n");
                                return;
                            }
                            else{
                                res = SUCCESS;
                                pid_t parent;
                                parent = getppid();
                                kill(parent, SIGCHLD);
                                svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
                                free(name);
                                if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &res)){
                                    printf("Ошибка отправки ответа (svc_sendreply)\n");
                                    return;
                                }
                                printf("svc_kill_process() успешно выполнена\n");
                                return;
                            }
                        }
                    }
                }
            }
            closedir(dir_proc);
            res = PROCESS_DOESNT_EXIST;
            if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &res)){
                printf("Ошибка отправки ответа (svc_sendreply)\n");
                return;
            }
            svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
            free(name);
            printf("Процесс не найден\n");
            return;
        }
        default:{
            svcerr_noproc(transp);
            return;
        }
    }
}

void sdf_run_process(struct svc_req *rqstp, SVCXPRT *transp){
    switch(rqstp->rq_proc){
        case NULLPROC:{
            if(!svc_sendreply(transp, (xdrproc_t) xdr_void, (caddr_t) 0)){
                printf("Ошибка отправки ответа (svc_sendreply)\n");
                return;
            }
        }
        case PROC_RUN:{
            static int result = 0;
            pid_t pid;

            char* name = (char*)malloc(200);
            memset(name, '\0', 100);
            svc_getargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);

            int pipfd[2];
            if(pipe(pipfd)){
                result = ERROR_PIPE;
                if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &result)){
                    printf("Ошибка отправки ответа (svc_sendreply)\n");
                }
                svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
                free(name);
                printf("Ошибка в создании PIPE\n");
                return;
            }

            if(fcntl(pipfd[1], F_SETFD, fcntl(pipfd[1], F_GETFD) | FD_CLOEXEC)){
                result = ERROR_FCNTL;
                if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &result)){
                    printf("Ошибка отправки ответа (svc_sendreply)\n");
                }
                svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
                free(name);
                printf("Ошибка fcntl\n");
                return;
            }

            pid = fork();
            if(pid == -1){
                result = ERROR_FORK;
                if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &result)){
                    printf("Ошибка отправки ответа (svc_sendreply)\n");
                }
                svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
                free(name);
                printf("Ошибка fork\n");
                return;
            }
            else{
                if(pid > 0){
                    struct sigaction sig;
                    sigemptyset(&sig.sa_mask);
                    sig.sa_handler = SIG_IGN;
                    sig.sa_flags = 0;
                    sigaction(SIGCHLD, &sig, NULL);

                    close(pipfd[1]);
                    ssize_t count;
                    int err;
                    while ((count = read(pipfd[0], &err, sizeof(errno))) == -1) {
                        if (errno != EAGAIN && errno != EINTR) {
                            break;
                        }
                    }
                    close(pipfd[0]);
                    if(!count){
                        result = SUCCESS;
                        if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &result)){
                            printf("Ошибка отправки ответа (svc_sendreply)\n");
                        }
                        svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
                        free(name);
                        printf("svc_run_process() успешно выполнена\n");
                        return;
                    }
                    else{
                        result = ERROR_EXEC;
                        if(!svc_sendreply(transp, (xdrproc_t) xdr_int, (caddr_t) &result)){
                            printf("Ошибка отправки ответа (svc_sendreply)\n");
                        }
                        svc_freeargs(transp, (xdrproc_t) xdr_wrapstring, (char*) &name);
                        free(name);
                        return;
                    }
                }
                else{
                    close(pipfd[0]);
                    int t = execlp(name, name, NULL);
                    if(t == -1){
                        write(pipfd[1], &errno, sizeof(int));
                        _exit(0);
                    }
                }
            }
        }
        default:{
            svcerr_noproc(transp);
            return;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////

////////////////////


