#ifndef MYRPC_H
#define MYRPC_H

#define PORTMAP
#include <rpc/pmap_clnt.h>
#include <rpc/pmap_prot.h>
#include <rpc/pmap_rmt.h>
#include <rpc/rpc.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>
#include <sys/time.h>
#include <sysexits.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip_icmp.h>
#include <netdb.h> //!
#include <rpcsvc/rusers.h> //!

#define PING_PKT_S 64
#define PORT_NO 0
#define PING_SLEEP_RATE 1000000
#define RECV_TIMEOUT 1



#define _POSIX_C_SOURCE 200809L

#define PROC_DIRECTORY "/proc/"
#define STR_LEN (int) 200
#define BUF_SIZE (int) 300000

#define ERROR_CALL_RPC ((int) -2)
#define PROCESS_DOESNT_EXIST ((int) -1)
#define ERROR_PID ((int) 0)
#define PROCESS_EXIST ((int) 1)
#define WRONG_DIRECTORY ((int) 2)
#define WRONG_ARGS ((int) 3)
#define ERROR_CHAR ((char*) "error")
#define ERROR_FORK ((int) 4)
#define ERROR_EXEC ((int) 5)
#define ERROR_PIPE ((int) 6)
#define ERROR_FCNTL ((int) 7)
#define SUCCESS ((int) 8)
#define ERROR_KILL ((int) 9)
#define END_CONNECTION ((int) 10)
#define ERROR_BREAKCONNECTION ((int) 11)
#define ERROR_CREATE_TCP ((int) 12)
#define ERROR_CALL_TCP ((int) 13)
#define ERROR_CLNT_CALL ((int) 14)


#define PROG_PROCESS_REQ ((u_long) 0x20000001)
#define VERS_PROCESS_REQ ((u_long) 1)
#define PROC_PROCESS_REQ ((u_long) 1)
	
#define PROG_INFO ((u_long) 0x20000003)
#define VERS_INFO ((u_long) 1)
#define PROC_INFO ((u_long) 1)

#define PROG_RUN ((u_long) 0x20000004)
#define VERS_RUN ((u_long) 1)
#define PROC_RUN ((u_long) 1)

#define PROG_KILL ((u_long) 0x20000005)
#define VERS_KILL ((u_long) 1)
#define PROC_KILL ((u_long) 1)

#define DEFAULT_SEC ((int) 5)
#define DEFAULT_USEC ((int) 0)

#ifdef __cplusplus
extern "C" {
#endif

struct ping_pkt
{
    struct icmphdr hdr;
    char msg[PING_PKT_S-sizeof(struct icmphdr)];
};


int IsNumeric(const char* CharacterList);

/*********************************************************************************************************************/
/*                                        Клиентские функции                                                         */
/*********************************************************************************************************************/

/*   Инициализирует структуру CLIENT* и timeval
 *   для выполнения последующих rpc запросов   */
bool client_init_connection(CLIENT** client, char* hostname, u_long prog_num,
                            u_long vers_num);

bool check_ping(char* hostname, char* wait_time, char* pack_number);

/*   Выполняет запрос данных обо всех процессах на сервере   */
char* client_all_process_req(struct timeval* total_timeout, CLIENT** client);


/*   Уничтожает структуру CLIENT* для безопасного разрыва соединения  */
void client_destroy_connection(CLIENT** client);


/*   Выполняет запрос данных о сервере   */
char* client_info_request(struct timeval* total_timeout, CLIENT** client);


/*   Выполняет запрос на убийство процесса с именем name   */
int client_kill(struct timeval* total_timeout, CLIENT** client, char* name);


/*   Выполняет запрос на запуск процесса с именем name   */
int client_run(struct timeval* total_timeout, CLIENT** client, char* name);


bool ping_connection(char* hostname);
unsigned short checksum(void *b, int len);
char *dns_lookup(char *addr_host, struct sockaddr_in *addr_con);
void send_ping(int ping_sockfd, struct sockaddr_in *ping_addr,
                char *ping_dom, char *ping_ip, char *rev_host);

/*********************************************************************************************************************/
/*                                             Серверные функции                                                     */
/*********************************************************************************************************************/

/* sdf = server dispatch function */

/*   Реализация запроса на получение данных о процессах
 *   на сервере и отправка ответа клиенту               */
void sdf_all_process_request(struct svc_req *rqstp, SVCXPRT *transp);


/*   Реализация запроса на получение данных о сервере и отправка ответа клиенту  */
void sdf_info_request(struct svc_req *rqstp, SVCXPRT *transp);


/*   Реализация запроса на уничтожение процесса на сервере и отправка ответа клиенту  */
void sdf_kill_process(struct svc_req *rqstp, SVCXPRT *transp);


/*   Реализация запроса на запуск процесса на сервере и отправка ответа клиенту  */
void sdf_run_process(struct svc_req *rqstp, SVCXPRT *transp);

#ifdef __cplusplus
}
#endif

#endif
