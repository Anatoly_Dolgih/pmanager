#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QChar>
#include <QDebug>
#include "processmodel.h"
#include "forthread.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    add = new add_server(this);
    all_ip = new QStringList();
    timer = new QTimer();
    params = new Settings(this);
    timer->setSingleShot(true);
    timer->setInterval(0);
    connect(add, SIGNAL(run_clicked(QString)), this, SLOT(register_ip(QString)));     /*   нажали кнопочку "ОК" при вводе Ip -> идем заполнять вкладку в register_ip()   */
    connect(add, SIGNAL(stop_clicked()), this, SLOT(stop()));       /*   нажали кнопочку "ОТМЕНА" при вводе Ip -> закрываем форму ввода Ip   */
    connect(this, SIGNAL(close_tab(int)), this, SLOT(on_Tabs_tabCloseRequested(int)));
    connect(timer, SIGNAL(timeout()), this, SLOT(connect_last_ip()));
    connect(this->params, SIGNAL(saveChanges(QVector<int>)), this, SLOT(SaveChangesMainSlot(QVector<int>)));

    mycount = 0;
    settings = new QSettings(this);

    struct sigaction sig;
    sigemptyset(&sig.sa_mask);
    sig.sa_handler = SIG_IGN;
    sig.sa_flags = 0;
    sigaction(SIGPIPE, &sig, NULL);


    start = true;
    loadSettings();
    timer->start();
}

MainWindow::~MainWindow()
{
    saveSettings();
    if(!TabSamples.isEmpty()){
        int size = TabSamples.size();
        for(int i = (size - 1); i >= 0; i--){
            delete TabSamples[i];
        }
        TabSamples.clear();
        TabSamples.squeeze();
    }
    all_ip->clear();
    delete timer;
    delete ui;
}

void MainWindow::saveSettings()
{
    settings->setValue("Geometry", geometry());
    settings->setValue("Last_IP", last_ip);
    QVector<int> temp = this->params->getParams();
    settings->setValue("Proc_time", temp[0]);
    settings->setValue("Wait_time", temp[1]);
    settings->setValue("Pack_number", temp[2]);
}

void MainWindow::loadSettings()
{
    setGeometry(settings->value("Geometry", QRect(0, 0, 1200, 800)).toRect());
    last_ip = (settings->value("Last_IP", "")).toString();
    this->params->setParams(settings->value("Proc_time", 3).toInt(),
                            settings->value("Wait_time", 10).toInt(),
                            settings->value("Pack_number", 3).toInt());
}


void MainWindow::on_action_new_connection_triggered()
{
    add->clear_all_fields();
    add->show();
}

void MainWindow::on_Tabs_tabCloseRequested(int index)
{
    if (ui->Tabs->count() <= 1)
        ui->Tabs->setDocumentMode(true);

    QString ip;
    if(index >= 0){
        ui->Tabs->setCurrentIndex(index);
        ui->Tabs->currentWidget()->deleteLater();
        ip = ui->Tabs->tabText(index);
        ui->Tabs->removeTab(index);
        ui->statusbar->showMessage(tr("Соединение разорвано"), 2000);
    }
    int check = all_ip->indexOf(ip);
    if(check != -1){
        all_ip->removeAt(check);
        last_ip = all_ip->takeLast();
    }
    else
        qDebug() << "Ошибка определения индекса удаляемого IP";
}


void MainWindow::on_action_break_connection_triggered()
{
    emit close_tab(ui->Tabs->currentIndex());
}

void MainWindow::on_action_break_all_connection_triggered()
{
    int count = ui->Tabs->count();
    all_ip->clear();
    for(int i = count; i > 0; i--){
        ui->Tabs->setCurrentIndex(i-1);
        ui->Tabs->currentWidget()->deleteLater();
        ip = ui->Tabs->tabText(i-1);
        ui->Tabs->removeTab(i-1);

    }
    ui->Tabs->setDocumentMode(true);
    last_ip = "127.0.0.1";
}

void MainWindow::register_ip(QString ip)
{
    QString msg = QString("Создается соединение с сервером ") + ip;
    add->close();
    if(!all_ip->contains(ip)){
        Tab_sample* tab = new Tab_sample(this, mycount, ip);
        tab->setparam(this->params->getParams());
        bool check = tab->check_connection();
        if(check){
            last_ip = ip;
            all_ip->append(ip);
            mycount++;
            if (ui->Tabs->documentMode() == true)
                ui->Tabs->setDocumentMode(false);
            this->TabSamples.push_back(tab);

            ui->Tabs->addTab(tab, ip);
            int index = ui->Tabs->count() - 1;
            ui->Tabs->setCurrentIndex(index);
        }
        else{
            delete tab;
        }

    }
    else{
        QString msg = QString("Вкладка для данного IP уже открыта\n(IP: ") + ip + QString(" )");
        QMessageBox::warning(this, "Ошибка", msg , QMessageBox::Ok);
        int index = all_ip->indexOf(all_ip->filter(ip)[0]);
        ui->Tabs->setCurrentIndex(index);
    }

}

void MainWindow::stop()
{
    add->close();
}

void MainWindow::connect_last_ip()
{
    QString msg = QString("Последним подключенным сервером был ") + last_ip + QString("\nПодключиться ?");
    QMessageBox messageBox(QMessageBox::Question,
                tr("Подключение"),
                msg,
                QMessageBox::Ok | QMessageBox::Cancel,
                this);
        messageBox.setButtonText(QMessageBox::Ok, "Да");
        messageBox.setButtonText(QMessageBox::Cancel, "Нет");
        messageBox.setDefaultButton(QMessageBox::Cancel);

    int res = messageBox.exec();

    switch(res)
    {
        case QMessageBox::Ok:
            if(!last_ip.isEmpty()){
                emit this->add->run_clicked(last_ip);
            }
            break;

        case QMessageBox::Cancel:
            break;
    }

}

void MainWindow::SaveChangesMainSlot(QVector<int> changes)
{
  for(int i = ui->Tabs->count(); i > 0; i--){
      connect(this, SIGNAL(SaveChangesTabSampleSignal(QVector<int>)), ui->Tabs->widget(i - 1),
              SLOT(SaveChangesSlotTabSample(QVector<int>)));
  }
  emit SaveChangesTabSampleSignal(changes);
}



void MainWindow::on_action_exit_triggered()
{
    this->close();
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    QMessageBox messageBox(QMessageBox::Question,
                tr("Выход"),
                tr("Уверены, что хотите выйти?"),
                QMessageBox::Ok | QMessageBox::Cancel,
                this);
        messageBox.setButtonText(QMessageBox::Ok, "Да");
        messageBox.setButtonText(QMessageBox::Cancel, "Нет");
        messageBox.setDefaultButton(QMessageBox::Cancel);

    int res = messageBox.exec();

    switch(res)
    {
        case QMessageBox::Ok:
            e->accept();
            break;

        case QMessageBox::Cancel:
            e->ignore();
            break;
    }
}

void MainWindow::on_action_triggered()
{
    params->show();
}
