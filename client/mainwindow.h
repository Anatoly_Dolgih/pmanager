#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <myrpc.h>
#include "tab_sample.h"
#include "add_server.h"
#include "forthread.h"
#include <QThread>
#include <QVariant>
#include <QCloseEvent>
#include <QSettings>
#include <settings.h>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void saveSettings();
    void loadSettings();


signals:
    void ip_done();     /* ip-адрес введен */
    void close_tab(int index);  /* Закрыть вкладку с индексом index */
    void start_connect_last_ip();
    void SaveChangesTabSampleSignal(QVector<int>);

private slots:    
    void on_action_new_connection_triggered();
    void on_Tabs_tabCloseRequested(int index);
    void on_action_break_connection_triggered();
    void on_action_break_all_connection_triggered();
    void register_ip(QString ip);
    void stop();
    void connect_last_ip();
    void SaveChangesMainSlot(QVector<int> changes);



    void on_action_exit_triggered();

    void on_action_triggered();

private:
    Ui::MainWindow *ui;
    add_server *add;    /* Окошко для ввода ip-адреса */
    QString ip;
    QVector<Tab_sample*> TabSamples;    /* Массив указателей на вкладки */
    QSettings *settings;
    QString last_ip;
    QStringList* all_ip;
    QTimer *timer;
    bool start;
    Settings *params;
    int mycount;
    //QVector<QString> all_ip;

protected:
    void closeEvent(QCloseEvent *e);

};
#endif // MAINWINDOW_H
