#include "forthread.h"
#include "myrpc.h"

ForThread::ForThread(QObject *parent) :
    QObject(parent)
{
    is_active = true;
    timer = nullptr;

    struct sigaction sig;
    client = NULL;
    sigemptyset(&sig.sa_mask);
    sig.sa_handler = SIG_IGN;
    sig.sa_flags = 0;
    sigaction(SIGPIPE, &sig, NULL);


}

void ForThread::sethostname(QString hostname)
{
    this->hostname = hostname;
}

void ForThread::thsetparams(QVector<int> params)
{
    this->params = params;
    total_timeout.tv_sec = params[0];
    timeofsleep = params[0] * 1000;
}

ForThread::~ForThread()
{
    client = nullptr;
    if(timer != nullptr)
        if(timer->isActive())
            timer->stop();
#ifdef QT_DEBUG
    qDebug() << "ForThread was deleted";
#endif
}

void ForThread::init(){
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(process_data()));
    CLIENT** p_client = &client;
    client_init_connection(p_client, hostname.toUtf8().data(),
                           PROG_PROCESS_REQ, VERS_PROCESS_REQ);      /* Инициализация this->client,
                                                                                         this->total_timeout */
    timer->start(timeofsleep);
    process_data();
}

void ForThread::process_data(){
    qDebug() << total_timeout.tv_sec << " " << timeofsleep;
    CLIENT** p_client = &client;
    struct timeval* p_total_timeout = &total_timeout;
    char result[BUF_SIZE];
    memset(result, '\0', 1);
    char* p_result = result;
    p_result = client_all_process_req(p_total_timeout, p_client);
    if(p_result != NULL){
        emit recvNewData(QString::fromUtf8(p_result));
    }
    else{
        client_destroy_connection(p_client);
        timer->stop();
        delete timer;
        emit finished();
    }
}

/*
void ForThread::changeActive(bool active)
{
    if(is_active == active)
        return;
    else
        is_active = active;
}
*/
void ForThread::SaveChangeSlotForThread(int timeout)
{
    timer->stop();
    timer->start(timeout * 1000);
    total_timeout.tv_sec = timeout;
    timeofsleep = 1000 * timeout;
    qDebug() << timeout * 1000;
}

void ForThread::DeleteTimerSlotForThread()
{
    if( timer != nullptr){
        CLIENT** p_client = &client;
        client_destroy_connection(p_client);
        timer->stop();
        delete timer;
        emit finished();
    }
}



