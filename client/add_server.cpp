#include "add_server.h"
#include "ui_add_server.h"
#include <QDebug>

add_server::add_server(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_server)
{
    ui->setupUi(this);
    QRegExp ip_reg("^(([0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\\.){3,3}"
                   "([0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]){1,1}$" );
    QRegExpValidator *ip_val = new QRegExpValidator(ip_reg, this);
    ui->ip_address->setValidator(ip_val);
}

add_server::~add_server()
{
    delete this->ui;
}

QString add_server::get_ip()
{
    m_ip = "";
    m_ip = ui->ip_address->text();
    return m_ip;
}


void add_server::clear_all_fields()
{
    ui->ip_address->clear();
    ui->ip_address->setFocus();
}

void add_server::on_run_clicked()
{
    if(ui->ip_address->hasAcceptableInput()){
        emit run_clicked(ui->ip_address->text());
    }
    else{
        QMessageBox::warning(this, "Внимание", "Неверно введен IP адрес", QMessageBox::Ok);
    }
}

void add_server::on_stop_clicked()
{
    emit stop_clicked();
}
