QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    add_server.cpp \
    processmodel.cpp \
    settings.cpp \
    tab_sample.cpp \
    forthread.cpp

HEADERS += \
    mainwindow.h \
    add_server.h \
    processmodel.h \
    settings.h \
    tab_sample.h \
    forthread.h

FORMS += \
    mainwindow.ui \
    add_server.ui \
    settings.ui \
    tab_sample.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../lib/release/ -lmyrpc
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../lib/debug/ -lmyrpc
else:unix: LIBS += -L$$PWD/../lib/ -lmyrpc

INCLUDEPATH += $$PWD/../lib
DEPENDPATH += $$PWD/../lib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../lib/release/libmyrpc.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../lib/debug/libmyrpc.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../lib/release/myrpc.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../lib/debug/myrpc.lib
else:unix: PRE_TARGETDEPS += $$PWD/../lib/libmyrpc.a

RESOURCES += \
    resource.qrc
