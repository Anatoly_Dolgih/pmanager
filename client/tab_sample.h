#ifndef TAB_SAMPLE_H
#define TAB_SAMPLE_H

#include <QWidget>
#include <QThread>
#include <QMessageBox>
#include <myrpc.h>
#include <processmodel.h>
#include <QVariant>
#include <QMovie>



class Ping : public QThread
{
    Q_OBJECT
public:
    explicit Ping(QMutex* mutex, bool* b);

    ~Ping(){ ping_mutex->unlock();};
    void run () override
    {
        ping_mutex->lock();
        *ipav = check_ping(hostname.toUtf8().data(),
                               QString::number(wait_time).toUtf8().data(),
                               QString::number(pack_number).toUtf8().data());
        qDebug() << "ipav: "<< ipav << *ipav;
        ping_mutex->unlock();
    }
    void set_data(QString hostname, int wait_time, int pack_number);
signals:
    void ResultPingSignalTabSample(bool res);
private:
    QString hostname;
    int wait_time;
    int pack_number;
    QMutex* ping_mutex;
    bool* ipav;
};


namespace Ui {
class Tab_sample;
}


class Tab_sample : public QWidget
{
    Q_OBJECT

public:
    explicit Tab_sample(QWidget *parent = nullptr, int index = 0, QString hostname = "");
    ~Tab_sample();

    void setparam(QVector<int> params);
    bool check_connection();
    bool checkPing();
    int getindex();

signals:
    void bad_connection(int index);
    void start_process_req();
    void SaveChangesSignalForThread(int timeout);

public slots:
    void SaveChangesSlotTabSample(QVector<int>);
    //void ResultPingSlotTabSample(bool res);
    //void StartCheckConnectionSlotTabSample();

private slots:
    void on_pushButton_run_clicked();
    void on_pushButton_delete_clicked();
    void on_pushButton_reconnect_clicked();
    void error_in_process_request();
    //void StartCheckConnectionSignalTabSample();


private:
    Ui::Tab_sample* ui;
    ProcessModel* processmodel;
    QString hostname;
    int index;
    CLIENT* client;
    struct timeval total_timeout;
    QVector<int> params;
    QMutex m_mutex;
    bool isIPAvailable;
    //Ping* m_ping;
};




#endif // TAB_SAMPLE_H
