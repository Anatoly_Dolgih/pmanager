#include "processmodel.h"
#include <QDebug>
#include <QString>
#include <QFlags>

ProcessModel::ProcessModel(QObject *parent, QString hostname)
    :QAbstractTableModel(parent),
      hostname(hostname)
{
    this->_rowCount = 0;
    this->_colCount = 6;

    m_thread = new QThread(this);
    back = new ForThread(); /* должен быть без родителя! */

    this->back->sethostname(this->hostname);

    connect(m_thread, &QThread::started, back, &ForThread::init);
    connect(back, &ForThread::finished, m_thread, &QThread::quit);
    connect(this, &QObject::destroyed, back, &QObject::deleteLater);
    connect(this, &ProcessModel::finish, m_thread, &QThread::quit);
    connect(m_thread, &QThread::finished, back, &ForThread::DeleteTimerSlotForThread);
    connect(back, SIGNAL(recvNewData(QString)), this, SLOT(test_recv_data(QString)));
}

ProcessModel::~ProcessModel()
{
    if(m_thread->isRunning()){
        m_thread->quit();
        m_thread->wait();
    }
    m_data.clear();
    m_data.squeeze();
    delete back;
    qDebug() << "ProcessModel was deleted";
}

void ProcessModel::set_row(int count)
{
    _rowCount = count;
}

int ProcessModel::rowCount(const QModelIndex &) const
{
    return _rowCount;
}

int ProcessModel::columnCount(const QModelIndex &) const
{
    return _colCount;
}

QVariant ProcessModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid()){
        return QVariant();
    }
    switch(role){
        case Qt::DisplayRole:
            return m_data.value(index.row()).value(index.column());
        case Qt::TextAlignmentRole:
            return int(Qt::AlignHCenter | Qt::AlignVCenter);
    }
    return QVariant();
}

QVariant ProcessModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole && orientation == Qt::Horizontal){
        switch(section){
        case 0:
            return QString("Real UID");
        case 1:
            return QString("PID процесса");
        case 2:
            return QString("VSZ");
        case 3:
            return QString("RSS");
        case 4:
            return QString("Статус");
        case 5:
            return QString("Имя процесса");
        }
    }
    return QVariant();
}

Qt::ItemFlags ProcessModel::flags(const QModelIndex &index) const
{
    if(!index.isValid()){
        return Qt::NoItemFlags;
    }
    return Qt::ItemIsEnabled | QAbstractTableModel::flags(index);
}

ForThread *ProcessModel::get_back()
{
    return back;
}

void ProcessModel::setparams(QVector<int> params)
{
    this->back->thsetparams(params);
}

void ProcessModel::process_table()
{
    back->moveToThread(m_thread);
    m_thread->start();
}

void ProcessModel::test_recv_data(QString data)
{
    m_data.clear();
    m_data.squeeze();
    QStringList list;
    list = data.split("\n\t");
    int count = 0;
    if(((list.size() - 1) % 6) == 0){
        while(count != (list.size() - 1)){
            QVector<QString> temp;
            temp.push_back(list.at(count));
            count++;
            temp.push_back(list.at(count));
            count++;
            for(int i = 0; i < 2; i++){
                if(list.at(count).contains("/") || list.at(count).contains("ff")){
                    temp.push_back("0");
                    count++;
                }
                else{
                    temp.push_back(list.at(count));
                    count++;
                }
            }
            temp.push_back(list.at(count));
            count++;
            temp.push_back(list.at(count));
            count++;
            m_data.push_back(temp);
     }
    _rowCount = m_data.size();
    emit layoutChanged();
    }
}

void ProcessModel::sethostname(QString hostname)
{
    this->hostname = hostname;
}
