#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    /* default values */
    proc_time = 3;
    wait_time = 10;
    pack_number = 3;
    ui->spinBox_proc->setValue(proc_time);
    ui->spinBox_wait->setValue(wait_time);
    ui->spinBox_count->setValue(pack_number);
}


Settings::~Settings()
{
    delete ui;
}

void Settings::setParams(int proc_time, int wait_time, int pack_number)
{
    this->proc_time = proc_time;
    this->wait_time = wait_time;
    this->pack_number = pack_number;
    ui->spinBox_proc->setValue(proc_time);
    ui->spinBox_wait->setValue(wait_time);
    ui->spinBox_count->setValue(pack_number);
}

int Settings::get_proc_time()
{
    return proc_time;
}

QVector<int> Settings::getParams()
{
    QVector<int> temp;
    temp.push_back(proc_time);
    temp.push_back(wait_time);
    temp.push_back(pack_number);
    return temp;
}

void Settings::on_pushButton_save_clicked()
{
    proc_time = ui->spinBox_proc->value();
    wait_time = ui->spinBox_wait->value();
    pack_number = ui->spinBox_count->value();
    QVector<int> temp;
    temp.push_back(proc_time);
    temp.push_back(wait_time);
    temp.push_back(pack_number);
    emit saveChanges(temp);
    this->close();
}

void Settings::on_pushButton_cancel_clicked()
{
    this->close();
}

void Settings::on_pushButton_clicked()
{
    proc_time = 3;
    wait_time = 10;
    pack_number = 3;
    ui->spinBox_proc->setValue(proc_time);
    ui->spinBox_wait->setValue(wait_time);
    ui->spinBox_count->setValue(pack_number);
}
