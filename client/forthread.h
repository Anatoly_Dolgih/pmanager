#ifndef FORTHREAD_H
#define FORTHREAD_H

#include "myrpc.h"
#include <QObject>
#include <QDebug>
#include <QTimer>
#include <QVariant>
#include <QMessageBox>
#include <QMutex>

class ForThread : public QObject
{
    Q_OBJECT

public:
    explicit ForThread(QObject *parent = nullptr);
    ~ForThread();

    void finish();      /* Очистка всех данных */
    void sethostname(QString hostname);     /* Задает поле hostname */
    void thsetparams(QVector<int> params);



signals:
    void finished();    /* Сбой сервера */
    void recvNewData(QString data);
    void del();


public slots:
    void init();     /* Постоянное обновление данных о процессах */
    void process_data();
    //void changeActive(bool active);     /* От сигнала о закрытии вкладки */
    void SaveChangeSlotForThread(int timeout);
    void DeleteTimerSlotForThread();

private:
    struct timeval total_timeout;
    CLIENT* client;
    bool is_active;
    QString hostname;
    QVector<int> params;
    QTimer *timer;
    int timeofsleep;
};

#endif // FORTHREAD_H
