#ifndef PROCESSMODEL_H
#define PROCESSMODEL_H

#include <QAbstractTableModel>
#include <QObject>
#include <QTime>
#include <QTimer>
#include <QDebug>
#include <QThread>
#include "forthread.h"
#include <QVariant>

class ProcessModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ProcessModel(QObject *parent = nullptr, QString hostname = "");
    ~ProcessModel();
    void set_row(int);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    ForThread *get_back();
    void setparams(QVector<int> params);

signals:
    void finish();

public slots:
    void process_table();
    void test_recv_data(QString data);
    void sethostname(QString hostname);

private:
    int _rowCount;
    int _colCount;
    QVector<QVector<QString>> m_data;
    ForThread* back;
    QThread* m_thread;
    QString hostname;
};

#endif // PROCESSMODEL_H
