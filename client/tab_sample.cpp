#include "tab_sample.h"
#include "ui_tab_sample.h"
#include <QDebug>
#include <QThread>
#include <QVariant>

Tab_sample::Tab_sample(QWidget *parent, int index, QString hostname) :
    QWidget(parent),
    ui(new Ui::Tab_sample),
    hostname(hostname), index(index)
{
    ui->setupUi(this);
    client = nullptr;
    total_timeout.tv_sec = 0;
    total_timeout.tv_usec = 0;
    processmodel = new ProcessModel(this, hostname);
    ui->tableView->setModel(processmodel);
    ui->tableView->verticalHeader()->hide();
    m_mutex.unlock();
    connect(this, SIGNAL(start_process_req()), this->processmodel, SLOT(process_table())); /* Запустить обработку данных в модели, т.к. есть соединение */
    connect(this->processmodel->get_back(), SIGNAL(finished()), this, SLOT(error_in_process_request()));
    connect(this, SIGNAL(SaveChangesSignalForThread(int)), this->processmodel->get_back(), SLOT(SaveChangeSlotForThread(int)));
}

Tab_sample::~Tab_sample()
{
    ui->tableView->clearSpans();
    delete ui;
    delete processmodel;
    qDebug() << "Tab_sample was deleted";
}

void Tab_sample::setparam(QVector<int> params)
{
    this->params = params;
    total_timeout.tv_sec = params[0];
    this->processmodel->setparams(params);
}

int Tab_sample::getindex()
{
    return index;
}


bool Tab_sample::check_connection()
{
    CLIENT** p_client = &client;
    struct timeval* p_total_timeout = &total_timeout;
    bool check = false;
    //QProgressDialog progress("Подключение к серверу", "Отмена", 0, n);
    Ping* m_ping = new Ping(&m_mutex, &isIPAvailable);
    m_ping->set_data(hostname, params[1], params[2]);
    m_ping->start();
    while(m_ping->isRunning()){
        qApp->processEvents();
    }
    if(isIPAvailable)
        check = client_init_connection(p_client, hostname.toUtf8().data(),
                                   PROG_INFO, VERS_INFO);
    else{
        QString msg = QString("IP адрес недоступен (ошибка ping)\n(IP: ") + hostname + QString(" )");
        QMessageBox::warning(this, "Ошибка", msg , QMessageBox::Ok);
        return false;
    }
    if(!check){
        ui->pushButton_reconnect->setEnabled(true);
        QString msg = QString("Не удалось подключиться к серверу\n(IP: ") + hostname + QString(" )");
        QMessageBox::warning(this, "Ошибка", msg , QMessageBox::Ok);
        ui->pushButton_delete->setEnabled(false);
        ui->pushButton_run->setEnabled(false);
        ui->groupBox_2->setEnabled(false);
        ui->tableView->setEnabled(false);
        ui->processname->setEnabled(false);
        ui->linePID->setEnabled(false);
        ui->processname->setEnabled(false);
        ui->label->setEnabled(false);
        ui->label_4->setEnabled(false);
        return false;
    }
    else{
        ui->pushButton_delete->setEnabled(true);
        ui->pushButton_run->setEnabled(true);
        ui->groupBox_2->setEnabled(true);
        ui->tableView->setEnabled(true);
        ui->tableView->setEnabled(true);
        ui->processname->setEnabled(true);
        ui->linePID->setEnabled(true);
        ui->processname->setEnabled(true);
        ui->label->setEnabled(true);
        ui->label_4->setEnabled(true);
        ui->pushButton_reconnect->setEnabled(false);
        char result[BUF_SIZE];
        char* p_result = result;
        p_result = client_info_request(p_total_timeout, p_client);

        if(p_result!= NULL){
            QString str = QString::fromUtf8(p_result);
            QString str1 = str.section('\n', 0, 0);
            QString str2 = str.section('\n', 1, 1);
            ui->CPU_line->setText(str1.section(": ", 1, 1));
            ui->RAM_line->setText(str2.section(":        ", 1, 1));
            emit start_process_req();
#ifdef QT_DEBUG
        qDebug() << "result info: " << str;
#endif
            return true;
        }
        QMessageBox::warning(this, "Ошибка", "Потеряно соединение", QMessageBox::Ok);
        ui->pushButton_delete->setEnabled(false);
        ui->pushButton_run->setEnabled(false);
        ui->groupBox_2->setEnabled(false);
        ui->tableView->setEnabled(false);
        ui->pushButton_reconnect->setEnabled(true);
        return false;

    }
}

void Tab_sample::on_pushButton_run_clicked()
{
    QString name = ui->processname->text();
    CLIENT* client;
    CLIENT** p_client = &client;
    struct timeval timeout;



    client_init_connection(p_client, hostname.toUtf8().data(),
                           PROG_RUN, VERS_RUN);
    int r = client_run(&timeout, p_client, name.toUtf8().data());
    qDebug() << r;
    switch(r){
        case ERROR_FORK:{
            QMessageBox::warning(this, "Результат", "Не удалось запустить процесс\n(ошибка при вызове fork())", QMessageBox::Ok);
            break;
        }
        case ERROR_EXEC:{
            QMessageBox::warning(this, "Результат", "Не удалось запустить процесс\n(ошибка при вызове exec())", QMessageBox::Ok);
            break;
        }
        case SUCCESS:{
            QMessageBox::information(this, "Результат", "Процесс успешно запущен", QMessageBox::Ok);
            break;
        }
        case ERROR_FCNTL:{
            QMessageBox::information(this, "Результат", "Не удалось запустить процесс\n(ошибка при вызове fcntl())", QMessageBox::Ok);
            break;
        }
        case ERROR_PIPE:{
            QMessageBox::information(this, "Результат", "Не удалось запустить процесс\n(ошибка при вызове pipe())", QMessageBox::Ok);
            break;
        }
        case ERROR_CLNT_CALL:{
            QMessageBox::information(this, "Результат", "Ошибка выполнения процедуры rpc\n(ошибка при вызове clnt_stat())", QMessageBox::Ok);
            break;
        }
    }
    

}

void Tab_sample::on_pushButton_delete_clicked()
{
    qDebug() << "Tab Sample" << params[0] << " " << params[1] << " " << params[2];
    QString name = ui->linePID->text();
    CLIENT* client;
    CLIENT** p_client = &client;
    struct timeval timeout;


    client_init_connection(p_client, hostname.toUtf8().data(),
                           PROG_KILL, VERS_KILL);
    int r = client_kill(&timeout, p_client, name.toUtf8().data());
    qDebug() << r;
    switch(r){
        case PROCESS_DOESNT_EXIST:{
            QMessageBox::warning(this, "Результат", "Не удалось удалить процесс\n(не существует процесса с данным именем)", QMessageBox::Ok);
            break;
        }
        case WRONG_DIRECTORY:{
            QMessageBox::warning(this, "Результат", "Не удалось удалить процесс\n(ошибка при обработке папки /proc)", QMessageBox::Ok);
            break;
        }
        case SUCCESS:{
            QMessageBox::information(this, "Результат", "Процесс успешно удален", QMessageBox::Ok);
            break;
        }
        case ERROR_CLNT_CALL:{
            QMessageBox::information(this, "Результат", "Ошибка выполнения процедуры rpc\n(ошибка при вызове clnt_stat())", QMessageBox::Ok);
            break;
        }
    }
    client_destroy_connection(p_client);
}


void Tab_sample::on_pushButton_reconnect_clicked()
{
    check_connection();
}

void Tab_sample::error_in_process_request()
{
    QMessageBox::warning(this, "Ошибка", "Не удалось получить данные от сервера\nПопытаемся заново подключиться", QMessageBox::Ok);
    check_connection();
}

void Tab_sample::SaveChangesSlotTabSample(QVector<int> temp)
{
    params = temp;
    emit SaveChangesSignalForThread(params[0]);
}

Ping::Ping(QMutex *mutex, bool *b)
    : ping_mutex(mutex), ipav(b)
{

}


void Ping::set_data(QString hostname, int wait_time, int pack_number)
{
    this->hostname = hostname;
    this->wait_time = wait_time;
    this->pack_number = pack_number;
}
