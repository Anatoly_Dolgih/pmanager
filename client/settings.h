#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = nullptr);
    ~Settings();

    void setParams(int proc_time, int wait_time, int pack_number);
    QVector<int> getParams();
    int get_proc_time();

signals:
    void saveChanges(QVector<int> changes);

private slots:
    void on_pushButton_save_clicked();
    void on_pushButton_cancel_clicked();

    void on_pushButton_clicked();

private:
    Ui::Settings *ui;
    int proc_time;
    int wait_time;
    int pack_number;
};

#endif // SETTINGS_H
