#ifndef ADD_SERVER_H
#define ADD_SERVER_H

#include <QDialog>
#include <QRegExpValidator>
#include <QRegExp>
#include <QMessageBox>
#include <QVariant>
#include <QValidator>

namespace Ui {
class add_server;
}

class add_server : public QDialog
{
    Q_OBJECT

public:
    explicit add_server(QWidget *parent = nullptr);
    ~add_server();

    QString get_ip();
    void clear_all_fields();     /*   Очищаем поле перед заполнением, чтобы не всплывали предыдущие Ip адреса   */

signals:
    void run_clicked(QString ip);
    void stop_clicked();

private slots:
    void on_run_clicked();
    void on_stop_clicked();

private:
    Ui::add_server *ui;
    QString m_ip;
};

#endif // ADD_SERVER_H
