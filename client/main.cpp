#include "mainwindow.h"
#include "tab_sample.h"
#include "add_server.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setOrganizationName("MySoft");
    QApplication::setApplicationName("PManager");
    MainWindow w;
    w.show();
    //w.connect_last_ip();
    return a.exec();
}
